/*
	1.) Create a function that returns a passed string with letters in alphabetical order.
		Example string: 'mastermind'
		Expected output: 'adeimmnrst'
	
*/

// Code here:
let myString = "mastermind";
console.log("%cFirst item","color:orange");

// Solution 1 without using split(), sort() and join() method
function sortMyString1(string) {
	let arrString = [];	
	//convert string to an array
	for( i = 0; i < string.length; i++){
		arrString.push(string[i]);
	}
	
	// sort the array
	for( x = 0; x < arrString.length; x++){
		for(y =x + 1 ; y <= arrString.length; y++){
			if(arrString[x] > arrString[y]){
				//get the value of array
				stringX = arrString[y];
				stringY = arrString[x];

				//swap the value of indexes
				arrString[y] = stringY;
				arrString[x] = stringX;
			}
		}
	}
	return myjoin(arrString); 
};

function myjoin(arrString){
	let newString='';
	
	// convert array of string into a string
	for(i = 0; i < arrString.length; i++){
		newString += arrString[i];
	}
	return newString;
};
console.log("%cSolution 1","color:limegreen");
console.log(sortMyString1(myString));



// Solution 2 using join() method
function sortMyString2(string) {
	let arrString = [];	
	//convert string to an array
	for( i = 0; i < string.length; i++){
		arrString.push(string[i]);
	}
	
	// sort the array
	for( x = 0; x < arrString.length; x++){
		for(y =x + 1 ; y <= arrString.length; y++){
			if(arrString[x] > arrString[y]){
				//get the value of array
				stringX = arrString[y];
				stringY = arrString[x];

				//swap the value of indexes
				arrString[y] = stringY;
				arrString[x] = stringX;
			}
		}
	}
	return arrString.join(""); 
}
console.log("%cSolution 2","color:limegreen");
console.log(sortMyString2(myString));



//Solution 3 using sort() and join() method
function  sortMyString3(string) {
	let arrString = [];	

	//convert string into an array so it can be sorted
	for( i = 0; i < string.length; i++){
		arrString.push(string[i]);
	}
	return arrString.sort().join("");
};
console.log("%cSolution 3","color:limegreen")
console.log(sortMyString3(myString));



//Solution 4 using split(""), sort() and join("") method
function sortMyString4(string){
	return string.split("").sort().join("");
};
console.log("%cSolution 4","color:limegreen");
console.log(sortMyString4(myString));

console.log("");


/*
	2.) Write a simple JavaScript program to join all elements of the following array into a string.

	Sample array : myColor = ["Red", "Green", "White", "Black"];
		Expected output: 
			Red,Green,White,Black
			Red,Green,White,Black
			Red+Green+White+Black
*/

// Code here:
const myArr = ["Darker", "than","black"];

console.log("%cSecond item","color:orange")
console.log(myArr.join());
console.log(myArr.join(','));
console.log(myArr.join('+'));

// this is a test/practise
console.log("%ctest/practise","color:pink")
function join(myArr){
	let arrOfjoin = [ ",", "+", ":", "-", "❤️"];

	console.log(myArr.join() + " //default join method");
	arrOfjoin.forEach(function (j){
		console.log(myArr.join(j))
	});

}
join(myArr);
console.log("");

/*
	3.) Write a function named birthdayGift that pass a gift as an argument.
		- if the gift is a stuffed toy, return a message that says: "Thank you for the stuffed toy, Michael!"
		- if the gift is a doll, return a message that says: "Thank you for the doll, Sarah!"
		- if the gift is a cake, return a message that says: "Thank you for the cake, Donna!"
		- if other gifts return a message that says: "Thank you for the (gift), Dad!"
		- create a global variable named myGift and invoke the function in the variable.
		- log the global var in the console

*/

// Code here:
function birthdayGift(gift){

	switch(gift){
		case "stuffed toy": return "Thank you for the stuffed toy, Michael!";

		case "doll": return "Thank you for the doll, Sarah!";

		case "cake": return "Thank you for the cake, Donna!";

		default: return "Thank you for the " + gift + ", Dad!";	
	}
}

console.log("%cThird item","color:orange");
console.log("%cSolution 1","color:limegreen");
let myGift = birthdayGift("stuffed toy");
console.log(myGift + "%c %s","font-size:20px","😘");

myGift = birthdayGift("doll");
console.log(myGift + "%c %s","font-size:20px","❤️");

myGift = birthdayGift("cake");
console.log(myGift + "%c %s","font-size:20px","❤️");

myGift = birthdayGift("house & lot");
console.log(myGift + "%c %s","font-size:20px","😱");



function birthdayGift2(gift){
	  return gift === "stuffed toy" ? "Thank you for the stuffed toy, Michael!"
        	: gift === "doll" ? "Thank you for the doll, Sarah!"
       		: gift === "cake"? "Thank you for the cake, Donna!"
        	: "Thank you for the " + gift + ", Dad!";
}
console.log("%cSolution 2","color:limegreen");

let myGift2 = birthdayGift2("stuffed toy");
console.log(myGift2 + "%c %s","font-size:20px","😘");

myGift2 = birthdayGift("doll");
console.log(myGift2 + "%c %s","font-size:20px","❤️");

myGift2 = birthdayGift("cake");
console.log(myGift2 + "%c %s","font-size:20px","❤️");

myGift2 = birthdayGift("house & lot");
console.log(myGift2 + "%c %s","font-size:20px","😱");
console.log("");
/*
	4.) Write a function that accepts a string as a parameter and counts the number of vowels within the string.

		Example string: 'The quick brown fox'
		Expected Output: 5

*/

// Code here:

let myString2 = "The quick brown fox";
let myVowels=[];
function countVowels(string){
	let count = 0;
	for(i = 0; i < string.length; i++){
	if (
 			string[i].toLowerCase() == "a" ||
 			string[i].toLowerCase() == "e" ||
 			string[i].toLowerCase() == "i" ||
 			string[i].toLowerCase() == "o" ||
 			string[i].toLowerCase() == "u"
 		){
 			count += 1;
 			myVowels.push(string[i]);
 		}
	}
	return count;
};

console.log("%cFourth item","color:orange");
console.log(`"${myString2}" string has a  %c${countVowels(myString2)}`,"color:limegreen;font-size:15px;","vowels!");
console.log("My Vowels are: " + myVowels);



